﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIII_IV
{
    class Jagging
    {
        public int[][] CopyJagged(int[][] srcArray) => srcArray.Select(s => s.ToArray()).ToArray();

        public static int[] CopyJag(int[] tab)
        {
            var tempArray = new int[tab.GetLength(0)];
            for (int i = 0; i < tab.GetLength(0); i++)
            {
                tempArray[i] = tab[i];
            }

            return tempArray;
        }
    }
}
